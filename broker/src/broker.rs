//! Message broker

use std::collections::HashMap;

pub struct MessageBroker {
    topics: HashMap<TopicName, Topic>,
    client_subscriptions: HashMap<ClientId, Subscriptions>,
}

pub type TopicName = String;

pub struct Topic {
    name: TopicName,
    policy: RetentionPolicy,
    message_log: Vec<LogEntry>,
    cur_index: Index,
}

#[derive(Copy, Clone)]
pub enum RetentionPolicy {
    RetainAll,
    Compact { max_size: usize },
}

pub struct LogEntry {
    index: Index,
    key: MessageKey,
    data: MessageData,
}

pub type MessageKey = String;
pub type MessageData = String;

pub type ClientId = String;

type Subscriptions = HashMap<TopicName, LastSeen>;

type Index = isize;
type LastSeen = Index;

#[allow(non_upper_case_globals)]
const NotSeenAny: LastSeen = -1;

impl MessageBroker {
    pub fn new() -> Self {
        MessageBroker {
            topics: HashMap::new(),
            client_subscriptions: HashMap::new(),
        }
    }

    pub fn create_topic(&mut self, topic_name: TopicName, policy: RetentionPolicy) {
        if self.topics.contains_key(&topic_name) {
            return;
        }
        let topic = Topic {
            name: topic_name.clone(),
            policy,
            message_log: Vec::new(),
            cur_index: 0,
        };
        self.topics.insert(topic_name, topic).ok_or(()).err().expect("internal error - key already exists");
    }

    pub fn subscribe(&mut self, client_id: &ClientId, topic_name: TopicName) {
        if let Some(subscriptions) = self.client_subscriptions.get_mut(client_id) {
            subscriptions.insert(topic_name, NotSeenAny);
        } else {
            let mut subscriptions = HashMap::new();
            subscriptions.insert(topic_name, NotSeenAny);
            self.client_subscriptions.insert(client_id.to_owned(), subscriptions);
        }
    }

    pub fn post_message(&mut self, topic_name: TopicName, message_key: MessageKey, message_data: MessageData) -> Result<(), ()> {
        let topic = self.topics.get_mut(&topic_name).ok_or(())?;
        debug_assert_eq!(topic_name, topic.name, "internal error - topic name mismatch");
        let log_entry = LogEntry {
            index: topic.cur_index,
            key: message_key,
            data: message_data,
        };
        topic.message_log.push(log_entry);
        topic.cur_index += 1;
        topic.process_compaction();
        Ok(())
    }

    pub fn update_client<F>(&mut self, client_id: &ClientId, mut handler: F)
        where F: FnMut(ClientId, TopicName, MessageKey, MessageData) -> Result<(), ()>
    {
        // If this client has active subscriptions
        if let Some(subscriptions) = self.client_subscriptions.get_mut(client_id) {
            // Loop over every subscription
            for (topic_name, last_seen) in subscriptions.iter_mut() {
                // Fetch corresponding topic
                if let Some(topic) = self.topics.get(topic_name) {
                    debug_assert_eq!(*topic_name, topic.name, "internal error - topic name mismatch");
                    // Send outstanding messages until either there are none or an error occured
                    Self::send_outstanding(topic, last_seen, |key, data| handler(client_id.clone(), topic_name.clone(), key.clone(), data.clone()));
                }
            }
        }
    }

    pub fn update_topic<F>(&mut self, topic_name: &TopicName, mut handler: F)
        where F: FnMut(ClientId, TopicName, MessageKey, MessageData) -> Result<(), ()>
    {
        // Fetch requested topic
        if let Some(topic) = self.topics.get(topic_name) {
            debug_assert_eq!(*topic_name, topic.name, "internal error - topic name mismatch");
            // Loop over every client that subscribed to that topic
            for (client_id, subscriptions) in self.client_subscriptions.iter_mut() {
                if let Some(last_seen) = subscriptions.get_mut(topic_name) {
                    // Send outstanding messages to this client until either there are none or an error occured
                    Self::send_outstanding(topic, last_seen, |key, data| handler(client_id.clone(), topic_name.clone(), key.clone(), data.clone()));
                }
            }
        }
    }

    fn send_outstanding<F>(topic: &Topic, last_seen: &mut isize, mut handler: F)
        where F: FnMut(&MessageKey, &MessageData) -> Result<(), ()>
    {
        for entry in topic.message_log.iter() {
            if entry.index > *last_seen {
                let res = handler(&entry.key, &entry.data);
                if res.is_err() {
                    break; // Error occured when sending update to the client
                }
                // Update subscription state if client notified successfully
                *last_seen = entry.index;
            }
        }
    }
}

impl Topic {
    fn process_compaction(&mut self) {
        match self.policy {
            RetentionPolicy::RetainAll => {},
            RetentionPolicy::Compact { max_size } => { Self::compact(&self.name, &mut self.message_log, max_size) },
        }
    }

    fn compact(name: &TopicName, log: &mut Vec<LogEntry>, max_size: usize) {
        if log.len() > max_size {
            log::info!("Performing compaction for topic {} (current size = {})", name, log.len());
            let mut last_index_for_key = HashMap::new();
            for entry in log.iter() {
                last_index_for_key.insert(entry.key.clone(), entry.index);
            }
            log.retain(|entry| entry.index >= *last_index_for_key.get(&entry.key).expect("internal error"));
            log::info!("Size after compaction: {}", log.len());
        }
    }
}

#[cfg(test)]
mod test {
    use crate::broker::{MessageBroker, RetentionPolicy};

    #[test]
    fn test_broker() {
        let mut broker = MessageBroker::new();

        broker.create_topic(s("Topic1"), RetentionPolicy::RetainAll);
        broker.post_message(s("Topic1"), s("Key1"), s("Data1")).unwrap();
        broker.post_message(s("Topic1"), s("Key1"), s("Data2")).unwrap();
        broker.post_message(s("Topic1"), s("Key2"), s("Data3")).unwrap();
        assert_eq!(broker.topics.get("Topic1").unwrap().message_log.len(), 3);

        broker.create_topic(s("Topic2"), RetentionPolicy::Compact { max_size: 3 });
        broker.post_message(s("Topic2"), s("key1"), s("1")).unwrap();
        broker.post_message(s("Topic2"), s("key1"), s("2")).unwrap();
        broker.post_message(s("Topic2"), s("key1"), s("3")).unwrap();
        assert_eq!(broker.topics.get("Topic2").unwrap().message_log.len(), 3);
        broker.post_message(s("Topic2"), s("key1"), s("4")).unwrap();
        assert_eq!(broker.topics.get("Topic2").unwrap().message_log.len(), 1);
        broker.post_message(s("Topic2"), s("key1"), s("5")).unwrap();
        assert_eq!(broker.topics.get("Topic2").unwrap().message_log.len(), 2);
        broker.post_message(s("Topic2"), s("key2"), s("6")).unwrap();
        assert_eq!(broker.topics.get("Topic2").unwrap().message_log.len(), 3);
        broker.post_message(s("Topic2"), s("key2"), s("7")).unwrap();
        assert_eq!(broker.topics.get("Topic2").unwrap().message_log.len(), 2);

        assert_eq!(broker.topics.get("Topic1").unwrap().message_log.len(), 3);

        broker.subscribe(&s("client1"), s("Topic1"));
        broker.subscribe(&s("client1"), s("Topic2"));
        broker.subscribe(&s("client2"), s("Topic2"));

        let mut messages = Vec::new();

        fn sort(data: &mut Vec<(String, String, String, String)>) {
            data.sort_by(
                |&(ref c1, ref t1, ref k1, ref m1), &(ref c2, ref t2, ref k2, ref m2)| {
                    std::cmp::Ordering::Equal
                        .then_with(|| c1.cmp(c2))
                        .then_with(|| t1.cmp(t2))
                        .then_with(|| k1.cmp(k2))
                        .then_with(|| m1.cmp(m2))
                }
            );
        }

        broker.update_client(&s("client1"), |client_id, topic_name, message_key, message_data| {
            assert_eq!(client_id, s("client1"));
            messages.push((client_id, topic_name, message_key, message_data));
            Ok(())
        });

        sort(&mut messages);

        assert_eq!(
            messages,
            vec![
                (s("client1"), s("Topic1"), s("Key1"), s("Data1")),
                (s("client1"), s("Topic1"), s("Key1"), s("Data2")),
                (s("client1"), s("Topic1"), s("Key2"), s("Data3")),
                (s("client1"), s("Topic2"), s("key1"), s("5")),
                (s("client1"), s("Topic2"), s("key2"), s("7")),
            ]
        );

        messages.clear();

        broker.update_topic(&s("Topic2"), |client_id, topic_name, message_key, message_data| {
            assert_eq!(topic_name, s("Topic2"));
            messages.push((client_id, topic_name, message_key, message_data));
            Ok(())
        });

        sort(&mut messages);

        assert_eq!(
            messages,
            vec![
                // Client 1 already received all message in previous update, so only client 2 here.
                /*(s("client1"), s("Topic2"), s("key1"), s("5")),
                (s("client1"), s("Topic2"), s("key2"), s("7")),*/
                (s("client2"), s("Topic2"), s("key1"), s("5")),
                (s("client2"), s("Topic2"), s("key2"), s("7")),
            ]
        );
    }

    fn s(s: &str) -> String {
        s.to_owned()
    }
}
