//! Entry point - message broker server

mod args;
mod protocol;
mod broker;
mod server;

fn main() -> Result<(), std::io::Error> {
    fern::Dispatch::new().chain(std::io::stderr()).apply().expect("failed to configure logger");
    let args = args::parse();
    let (ip, port) = args.ip_port();
    let server = server::Server::new(&ip, port)?;
    server.listen()?;
    Ok(())
}
