//! Message broker server

use std::collections::HashMap;
use std::io::{BufRead, BufReader, Error, ErrorKind, Write};
use std::net::{IpAddr, SocketAddr, TcpListener, TcpStream};
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::thread;
use crate::broker::{ClientId, MessageBroker, MessageData, MessageKey, TopicName};
use crate::protocol::{Command, SubscriptionMessage};

pub struct Server {
    listener: TcpListener,
    clients: SharedWritableClients,
    broker: SharedBroker,
}

struct Client {
    client_id: ClientId,
    clients: SharedWritableClients,
    broker: SharedBroker,
}

struct WritableClients (HashMap<ClientId, TcpStream>);

type SharedWritableClients = Arc<Mutex<WritableClients>>;

type SharedBroker = Arc<Mutex<MessageBroker>>;

impl Server {
    pub fn new(ip: &str, port: u16) -> Result<Self, Error> {
        log::info!("Listening on {}:{}", ip, port);
        let ip = IpAddr::from_str(ip).map_err(|e| Error::new(ErrorKind::InvalidInput, e))?;
        let addr = SocketAddr::new(ip, port);
        let listener = TcpListener::bind(addr)?;
        let clients = Arc::new(Mutex::new(WritableClients::new()));
        let broker = Arc::new(Mutex::new(MessageBroker::new()));
        Ok(Server { listener, clients, broker })
    }

    pub fn listen(&self) -> Result<(), Error> {
        for res in self.listener.incoming() {
            let stream = res?;
            self.handle_connection(stream)?;
        }
        Ok(())
    }

    fn handle_connection(&self, stream: TcpStream) -> Result<(), Error> {
        let client_id = stream.peer_addr()?.to_string();
        let client = Client::new(client_id, self.clients.clone(), self.broker.clone());
        thread::spawn(move || {
            client.run(stream);
        });
        Ok(())
    }
}

impl Client {
    fn new(client_id: ClientId, clients: SharedWritableClients, broker: SharedBroker) -> Self {
        Client { client_id, clients, broker }
    }

    fn run(&self, stream: TcpStream) {
        log::info!("Client connected: {}", self.client_id);

        let write_strteam = stream.try_clone().expect("handle clone failed");
        let read_stream = stream;

        { // Explicit scope to minimize mutex lock time
            let mut clients = self.clients.lock().expect("broken mutex");
            clients.client_connected(&self.client_id, write_strteam);
        }

        let res = self.process_commands(read_stream);
        if let Some(err) = res.err() {
            self.handle_error(err);
        }

        { // Explicit scope to minimize mutex lock time
            let mut clients = self.clients.lock().expect("broken mutex");
            clients.client_disconnected(&self.client_id);
        }

        log::info!("Client disconnected: {}", self.client_id);
    }

    fn process_commands(&self, read_stream: TcpStream) -> Result<(), Error> {
        let mut reader = BufReader::new(read_stream);
        let mut buf = String::new();
        loop {
            reader.read_line(&mut buf)?;
            if buf.is_empty() {
                break;
            }
            self.handle_command(&buf)?;
            buf.clear();
        }
        Ok(())
    }

    fn handle_command(&self, cmd: &str) -> Result<(), Error> {
        let cmd = serde_json::from_str::<Command>(cmd).map_err(|e| Error::new(ErrorKind::InvalidData, e))?;
        let client_id = &self.client_id;
        let mut broker = self.broker.lock().expect("broken mutex");
        match cmd {
            Command::CreateTopic { topic_name, retention_policy } => {
                log::info!("Creating topic: {} with policy {:?}", topic_name, retention_policy);
                broker.create_topic(topic_name, retention_policy.into())
            },
            Command::Post { topic_name, message_key, message_data } => {
                log::info!("Received message in {} topic: {} => {}", topic_name, message_key, message_data);
                broker.post_message(topic_name.clone(), message_key, message_data).map_err(|_| Error::from(ErrorKind::InvalidData))?;
                let mut clients = self.clients.lock().expect("broken mutex");
                broker.update_topic(&topic_name, |client_id, topic_name, message_key, message_data| clients.client_write(&client_id, topic_name, message_key, message_data).map_err(|_| ()));
            },
            Command::Subscribe { topic_name } => {
                log::info!("Client {} subscribed to {}", client_id, topic_name);
                broker.subscribe(client_id, topic_name);
                let mut clients = self.clients.lock().expect("broken mutex");
                broker.update_client(client_id, |client_id, topic_name, message_key, message_data| clients.client_write(&client_id, topic_name, message_key, message_data).map_err(|_| ()));
            },
        }
        Ok(())
    }

    fn handle_error(&self, err: Error) {
        log::info!("Remote client {} error: {:?}", self.client_id, err);
    }
}

impl WritableClients {
    fn new() -> Self {
        WritableClients(HashMap::new())
    }

    fn client_connected(&mut self, client_id: &ClientId, write_stream: TcpStream) {
        let WritableClients(map) = self;
        map.insert(client_id.to_owned(), write_stream);
    }

    fn client_disconnected(&mut self, client_id: &ClientId) {
        let WritableClients(map) = self;
        map.remove(client_id);
    }

    fn client_write(&mut self, client_id: &ClientId, topic_name: TopicName, message_key: MessageKey, message_data: MessageData) -> Result<(), Error> {
        let msg = SubscriptionMessage { topic_name, message_key, message_data };
        let mut msg = serde_json::to_string(&msg).expect("serialization error");
        msg.push('\n');

        let WritableClients(map) = self;
        if let Some(stream) = map.get_mut(client_id) {
            stream.write_all(msg.as_bytes())?;
            stream.flush()?;
        }

        Ok(())
    }
}
