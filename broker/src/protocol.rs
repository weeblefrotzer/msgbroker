//! Client-server protocol model

use serde::{Serialize, Deserialize};

#[derive(Deserialize)]
#[serde(tag = "command")]
pub enum Command {
    #[serde(rename = "create")]
    CreateTopic {
        #[serde(rename = "topic")]
        topic_name: String,

        #[serde(rename = "policy")]
        retention_policy: RetentionPolicy,
    },

    #[serde(rename = "post")]
    Post {
        #[serde(rename = "topic")]
        topic_name: String,

        #[serde(rename = "message_key")]
        message_key: String,

        #[serde(rename = "message_data")]
        message_data: String,
    },

    #[serde(rename = "subscribe")]
    Subscribe {
        #[serde(rename = "topic")]
        topic_name: String,
    },
}

#[derive(Deserialize, Debug)]
pub enum RetentionPolicy {
    #[serde(rename = "retain")]
    RetainAll,
    #[serde(rename = "compact")]
    Compact {
        #[serde(rename = "size")]
        max_size: usize
    },
}

#[derive(Serialize)]
pub struct SubscriptionMessage {
    #[serde(rename = "topic")]
    pub topic_name: String,

    #[serde(rename = "message_key")]
    pub message_key: String,

    #[serde(rename = "message_data")]
    pub message_data: String,
}

impl From<RetentionPolicy> for crate::broker::RetentionPolicy {
    fn from(policy: RetentionPolicy) -> Self {
        match policy {
            RetentionPolicy::RetainAll => crate::broker::RetentionPolicy::RetainAll,
            RetentionPolicy::Compact { max_size } => crate::broker::RetentionPolicy::Compact { max_size },
        }
    }
}