//! Command line arguments parsing

use structopt::StructOpt;

/// Message broker server.
#[derive(StructOpt)]
pub struct Opts {
    /// IP address to bind to
    #[structopt(short = "i", long = "interface", name = "LOCAL IP")]
    ip: Option<String>,

    /// Port to listen on
    #[structopt(short = "p", long = "port", name = "PORT")]
    port: Option<u16>,
}

impl Opts {
    pub fn ip_port(&self) -> (String, u16) {
        let ip = self.ip.as_ref().map(String::as_ref).unwrap_or_else(|| "0.0.0.0");
        let port = self.port.unwrap_or(shared::DEFAULT_PORT);
        (ip.to_string(), port)
    }
}

pub fn parse() -> Opts {
    Opts::from_args()
}
