//! Client-server protocol model

use serde::{Serialize, Deserialize};

#[derive(Serialize)]
#[serde(tag = "command")]
pub enum Command<'a> {
    #[serde(rename = "create")]
    CreateTopic {
        #[serde(rename = "topic")]
        topic_name: &'a str,

        #[serde(rename = "policy")]
        retention_policy: RetentionPolicy,
    },

    #[serde(rename = "post")]
    Post {
        #[serde(rename = "topic")]
        topic_name: &'a str,

        #[serde(rename = "message_key")]
        message_key: &'a str,

        #[serde(rename = "message_data")]
        message_data: &'a str,
    },

    #[serde(rename = "subscribe")]
    Subscribe {
        #[serde(rename = "topic")]
        topic_name: &'a str,
    },
}

#[derive(Serialize)]
pub enum RetentionPolicy {
    #[serde(rename = "retain")]
    RetainAll,
    #[serde(rename = "compact")]
    Compact {
        #[serde(rename = "size")]
        max_size: usize
    },
}

#[derive(Deserialize)]
pub struct SubscriptionMessage {
    #[serde(rename = "topic")]
    pub topic_name: String,

    #[serde(rename = "message_key")]
    pub message_key: String,

    #[serde(rename = "message_data")]
    pub message_data: String,
}
