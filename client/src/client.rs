//! Message broker client

use std::io::{BufRead, BufReader, Error, ErrorKind, Write};
use std::net::{IpAddr, SocketAddr, TcpStream};
use std::str::FromStr;
use crate::protocol::{Command, SubscriptionMessage, RetentionPolicy};

pub struct Client (TcpStream);

impl Client {
    pub fn connect(host: &str, port: u16) -> Result<Self, Error> {
        let ip = IpAddr::from_str(host).map_err(|e| Error::new(ErrorKind::InvalidInput, e))?;
        let addr = SocketAddr::new(ip, port);
        log::info!("Connecting to: {}", addr);
        let stream = TcpStream::connect(addr)?;
        Ok(Client(stream))
    }

    pub fn create_topic(&mut self, topic_name: &str, compact_max_size: Option<usize>) -> Result<(), Error> {
        log::info!("Creating topic: {}", topic_name);
        let retention_policy = if let Some(max_size) = compact_max_size { RetentionPolicy::Compact { max_size } } else { RetentionPolicy::RetainAll };
        let cmd = Command::CreateTopic { topic_name, retention_policy };
        let cmd = serde_json::to_string(&cmd).expect("serialization error");
        self.send(cmd)
    }

    pub fn post_message(&mut self, topic_name: &str, message_key: &str, message_data: &str) -> Result<(), Error> {
        log::info!("Sending message to topic {}: {} => {}", topic_name, message_key, message_data);
        let cmd = Command::Post { topic_name, message_key, message_data };
        let cmd = serde_json::to_string(&cmd).expect("serialization error");
        self.send(cmd)
    }

    pub fn subscribe(&mut self, topic_name: &str) -> Result<(), Error> {
        log::info!("Subscribing to {}", topic_name);
        let cmd = Command::Subscribe { topic_name };
        let cmd = serde_json::to_string(&cmd).expect("serialization error");
        self.send(cmd)?;
        self.receive_loop()
    }

    fn send(&mut self, mut cmd: String) -> Result<(), Error> {
        cmd.push('\n');
        let Client(stream) = self;
        stream.write_all(cmd.as_bytes())?;
        stream.flush()
    }

    fn receive_loop(&mut self) -> Result<(), Error> {
        let Client(stream) = self;
        let mut reader = BufReader::new(stream);
        let mut buf = String::new();
        loop {
            reader.read_line(&mut buf)?;
            if buf.is_empty() {
                break;
            }
            Self::handle_message(&buf)?;
            buf.clear();
        }
        Ok(())
    }

    fn handle_message(msg: &str) -> Result<(), Error> {
        let msg = serde_json::from_str::<SubscriptionMessage>(msg).map_err(|e| Error::new(ErrorKind::InvalidData, e))?;
        log::info!("Received message in topic {}: {} => {}", msg.topic_name, msg.message_key, msg.message_data);
        Ok(())
    }
}
