//! Command line arguments parsing

use structopt::StructOpt;

/// Message broker client.
/// Try this: client --create --topic test_topic --compact 10 --send --key foo --message "Test message" --subscribe
#[derive(StructOpt)]
pub struct Opts {
    /// IP address to connect to, default 127.0.0.1
    #[structopt(short = "h", long = "host", name = "REMOTE IP")]
    host: Option<String>,

    /// Port to connect to, default 1234
    #[structopt(short = "p", long = "port", name = "PORT")]
    port: Option<u16>,

    /// Topic name
    #[structopt(short = "t", long = "topic", name = "TOPIC NAME")]
    pub topic: Option<String>,

    /// Retention policy: compact with specified max size
    #[structopt(short = "a", long = "compact", name = "MAX SIZE")]
    pub compact: Option<usize>,

    /// Message key
    #[structopt(short = "k", long = "key", name = "MESSAGE KEY")]
    pub key: Option<String>,

    /// Message data
    #[structopt(short = "m", long = "message", name = "MESSAGE TEXT")]
    pub message: Option<String>,

    /// Create topic command
    #[structopt(short = "c", long = "create")]
    pub create_topic: bool,

    /// Post message command
    #[structopt(short = "s", long = "send")]
    pub post_message: bool,

    /// Subscribe to topic command
    #[structopt(short = "u", long = "subscribe")]
    pub subscribe: bool,
}

impl Opts {
    pub fn host_port(&self) -> (String, u16) {
        let host = self.host.as_ref().map(String::as_ref).unwrap_or_else(|| "127.0.0.1");
        let port = self.port.unwrap_or(shared::DEFAULT_PORT);
        (host.to_string(), port)
    }
}

pub fn parse() -> Opts {
    Opts::from_args()
}
