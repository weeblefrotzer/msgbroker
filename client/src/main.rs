//! Entry point - message broker client

mod args;
mod protocol;
mod client;

fn main() -> Result<(), std::io::Error> {
    fern::Dispatch::new().chain(std::io::stderr()).apply().expect("failed to configure logger");

    let args = args::parse();

    let (host, port) = args.host_port();
    let mut client = client::Client::connect(&host, port)?;

    if args.create_topic {
        let topic_name = args.topic.as_ref().expect("topic name expected");
        let compact_max_size = args.compact;
        client.create_topic(topic_name, compact_max_size)?;
    }

    if args.post_message {
        let topic_name = args.topic.as_ref().expect("topic name expected");
        let message_key = args.key.as_ref().expect("message key expected");
        let message_data = args.message.as_ref().expect("message text expected");
        client.post_message(topic_name, message_key, message_data)?;
    }

    if args.subscribe {
        let topic_name = args.topic.as_ref().expect("topic name expected");
        client.subscribe(topic_name)?;
    }

    Ok(())
}
