# Message Broker (client & server)

Игрушечный брокер сообщений.

## Сборка и запуск

После сборки проекта получаем два бинарника: `broker` и `client`:

```
$ cargo build

$ cd target/debug

$ broker --help
USAGE:
    broker [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --interface <LOCAL IP>    IP address to bind to
    -p, --port <PORT>             Port to listen on

$ client --help
USAGE:
    client [FLAGS] [OPTIONS]

FLAGS:
    -c, --create       Create topic command
        --help         Prints help information
    -s, --send         Post message command
    -u, --subscribe    Subscribe to topic command
    -V, --version      Prints version information

OPTIONS:
    -a, --compact <MAX SIZE>        Retention policy: compact with specified max size
    -k, --key <MESSAGE KEY>         Message key
    -m, --message <MESSAGE TEXT>    Message data
    -p, --port <PORT>               Port to connect to, default 1234
    -h, --host <REMOTE IP>          IP address to connect to, default 127.0.0.1
    -t, --topic <TOPIC NAME>        Topic name
```

Запускаем сервер:

```
$ broker
Listening on 0.0.0.0:1234
```

Далее с помощью клиента создаём пару топиков:

```
$ client --create --topic Test1
Connecting to: 127.0.0.1:1234
Creating topic: Test1

$ client --create --topic Test2 --compact 5
Connecting to: 127.0.0.1:1234
Creating topic: Test2
```

В первом случае создаётся топик с политикой `RetainAll`, т.е. все сообщения в логе сохраняются.
Во втором случае топик создаётся с политикой `Compact when size > 5`.

Подпишемся на второй топик:

```
$ client --subscribe --topic Test2
Connecting to: 127.0.0.1:1234
Subscribing to Test2
```

Теперь ещё одним клиентом будем отправлять сообщения в этот топик:

```
$ client --send --topic Test2 --key FOO --message "Message 1"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 1

$ client --send --topic Test2 --key FOO --message "Message 2"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 2

$ client --send --topic Test2 --key FOO --message "Message 3"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 3

$ client --send --topic Test2 --key FOO --message "Message 4"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 4

$ client --send --topic Test2 --key FOO --message "Message 5"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 5

$ client --send --topic Test2 --key BAR --message "Message 6"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: BAR => Message 6

$ client --send --topic Test2 --key FOO --message "Message 7"
Connecting to: 127.0.0.1:1234
Sending message to topic Test2: FOO => Message 7
```

Первый клиент (подписанный на топик) выводит получаемые сообщения:
```
Received message in topic Test2: FOO => Message 1
Received message in topic Test2: FOO => Message 2
Received message in topic Test2: FOO => Message 3
Received message in topic Test2: FOO => Message 4
Received message in topic Test2: FOO => Message 5
Received message in topic Test2: BAR => Message 6
Received message in topic Test2: FOO => Message 7
```

Сервер также выводит в консоль статусные сообщения.
После отправки сообщения №6 происходит сжатие лога:
```
Received message in Test2 topic: BAR => Message 6
Performing compaction for topic Test2 (current size = 6)
Size after compaction: 2
```

Теперь запустим ещё одного клиента с подпиской на этот топик:
```
$ client --subscribe --topic Test2
Connecting to: 127.0.0.1:1234
Subscribing to Test2
Received message in topic Test2: FOO => Message 5
Received message in topic Test2: BAR => Message 6
Received message in topic Test2: FOO => Message 7
```

Как видим, данный клиент получил сообщения только с момента сжатия: для ключа FOO это было сообщение №5, для ключа BAR - сообщение №6, позднее было добавлено сообщение №7.

## Замечания по исходному коду

Сетевая часть проекта была выполнена с использованием `std::net` без привлечения сторонних крейтов.
На стороне сервера на каждое входящее соединение создаётся отдельный поток.
Для игрушечного сервера такой подход вполне оправдан.
Рассылка уведомлений подписанным клиентам происходит в контексте того потока, чей клиент прислал обновление.
Это простейший подход, опять же, вполне подходящий для игрушечного проекта.

Протокол общения между клиентом и сервером - JSON-сообщения, разделённые символом перевода строки.

Логика управления логом сообщений находится в файле `broker.rs`, сетевой код сервера - в файле `server.rs`, сетевой код клиента - `client.rs`.
Остальные модули выполняют вспомогательную роль: разбор аргументов командной строки (`args.rs`), точки входа в программу (`main.rs`), сетевой протокол (`protocol.rs`).
